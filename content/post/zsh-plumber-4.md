---
title: "Zsh Prototype(?) 2.2"
date: 2020-04-09T13:16:32-05:00
draft: true
tags: []
---

_[all posts in this series](/tags/zsh-plumber)_

[The fourth prototype is done,
and I've solved some more problems](https://github.com/xPMo/zsh-plumber/tree/proto-4).
Again, this is an iteration on prototype 2.

<!--more-->

I switched back to setting `$BUFFER` instead of using `eval`.
Part of the reason I didn't want to use `$BUFFER` was that
after the command is run,
the `$BUFFER` is visible in the previous line.
To make this problem less prominent,
I moved our messy stuff into a new function,
meaning after `some | pipeline |` `<Enter>`,
the line will look like

```
❯ some | pipeline > >(→plumber-relay)
```

Additionally, I figured out how to suppress history!
We push the history to the stack using `fc -p`,
let Zsh add the command line to a new history list,
then pop the history with `fc -P` after the command finishes.
Finally, `print -rs "$_ZPLUMBER_BUFFER"` in the `line-init` hook.

Tests are still out of date,
and now that we just use `$BUFFER` it will be easier to rewrite them.
I'll probably do that next,
and may set the `proto-4` tag to after the test-rewrites
(and corresponding fixes).

