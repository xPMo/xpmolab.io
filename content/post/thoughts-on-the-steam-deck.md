---
title: "Thoughts on the Steam Deck"
date: 2021-07-29T10:02:36-05:00
tags: ["gaming", "linux"]
---

As a person who has been daily-driving Linux for about five years now,
I am understandably excited for the upcoming Steam Deck
and what it means for gaming on Linux in general.
I just want to have some place to collect my thoughts
so I could easily share what I thought of it.

<!--more-->

# Caveats

Every opinion has bias; mine is no different.
You may want to know:

- I run (Arch) Linux (btw).
- While I have a "gaming PC",
it is quite old and _less powerful than_ the Steam Deck.
- I am a heavy Steam Controller user,
and especially love the touchpads.
- I own a Switch.
- I spend my gaming time roughly equally between
my Switch, my desktop, and my laptop.
- My game interests fall almost entirely within two genres:
Strategy and Platforming.

# My Thoughts

## Hardware

My current desktop has a FX-6300 and an HD 6970.
In games which do run, I am usually CPU-bottlenecked.
However, newer games simply don't work or have corrupted graphics
due to my card's age.

The Deck uses the latest RDNA 2, so the graphics support is as future-proof as possible.
The Zen 2 CPU isn't the latest,
but is more than enough power for an mobile APU.
As someone who is an infrequent upgrader,
I absolutely love the balance provided here.

Additionally, this combination of Zen 2 and  RDNA 2
is strictly better than any other commercially available APU.

The screen is 1280x800,
which is appropriate for its dimensions and the power provided to it.
A 1080p screen has more than twice the pixels
and so would tax the system far more than the quality improvement offered.
If a user wants to drop the resolution,
upscaling artifacts would significantly degrade the experience.

The screen is not VRR (more on that later).

_Remember that external hardware can be attached._

Having used a Switch and a Steam Controller significantly,
the Deck looks designed for comfort.
I bought the [Hori Split Pad Pro](https://www.amazon.com/gp/product/B08FJ8B197/ref=ppx_yo_dt_b_asin_title_o01_s00)
which is far more comfortable,
although is only useful when attached to the Switch.

The Steam Controller's shape guides the user's fingers
to rest on the bumpers, triggers, and grip buttons.
I expect the Steam Deck to be designed with this in mind.

## SteamOS 3.0

I already run Arch,
so the change from a Debian base to an Arch base is welcome.
Valve does a lot of work with the Linux community
(Mesa, KWin, the kernel, Wine, DXVK)
and these contributions would be more easily included in SteamOS
if it tracked upstream more closely.
Arch fits this criteria perfectly.

The choice of KDE is makes sense for a community who primarily uses Windows.
KDE relies on many of the same paradigms as Windows does,
and it is one of the two most widely adopted and supported desktop environments on Linux.

My biggest worry here is with Wayland. Will SteamOS use Wayland?

> For those who don't know,
[Wayland](https://wayland.freedesktop.org/) is a new display server protocol,
intended to replace X11.
Unlike X11, a Wayland display server is _also_ a window manager and a compositor.
All common Linux X11 desktops use X.org as their display server.
The Wayland protocol has been in development since 2008,
but mature implementations of the protocol did not appear until much more recently.

> Older programs written for X11 can use a modified X.org server called Xwayland
which talks X11 with its clients and talks Wayland with the Wayland compositor.
X.org itself has ceased development,
so it is clear that Wayland is the future of the Linux desktop.

The KDE project is not confident in its Wayland implementation at the moment
(["Wayland support in the KDE Plasma Workspaces is in a tech-preview state."](https://community.kde.org/KWin/Wayland))
although this line has not been altered for years since work began.

Even if KDE is confident in its Wayland implementation,
it is still very new and most games will run with Xwayland instead.
Valve seems confident enough that Steam will work well on top of KDE,
so perhaps I don't need to worry here.

Personally, I'm curious how Sway will play with the touchscreen and touchpad input.

## Proton

Valve has reportedly encouraged developers to target Proton
_instead of_ targeting Linux natively,
which has raised some justified concerns to the long-term health of gaming on Linux.
I understand why this was done: performance in Linux ports wildly vary,
with a large number having significantly better performance
or having more features on Proton.
I hope that despite this,
developers will still look for performance improvements offered
by making native Linux builds
using tools such as [DXVK-native](https://github.com/Joshua-Ashton/dxvk-native).

Finally, Valve is implementing anti-cheat support into Proton
using the new [Syscall User Dispatch](https://www.kernel.org/doc/html/latest/admin-guide/syscall-user-dispatch.html) feature in the Linux kernel.
This is integral to reaching 100% library compatibility.

## Criticisms

Most of the hardware choice criticisms are validated by the price point.

The lack of VRR on the Deck's builtin screen is the hardest to justify.
Even though I have never had the opportunity to use a VRR screen,
I am on board with this criticism.
This decision was probably made with a _heated_ internal discussion.

Here are possible points for not including VRR:
- _Price (obviously)._
Not compromising here would mean compromising elsewhere.
The \$399 target was very important for Valve.
- _Less effective when trying to save power._
Because gaming in handheld mode means gaming with power concerns,
users in handheld mode would want to limit framerates.
Users who limit their games to 30 fps would be giving up the advantages of VRR.
Now with VRR, users could target 40 or 50 fps instead,
but the fact that many users will framerate limit to 30 or 60 fps makes VRR less worth it.
- _Less effective when not trying to save power._
If a user doesn't have power concerns,
that likely means that the Deck is docked.
And if the Deck is docked,
then it almost certainly plugged into an external monitor
where the actual gameplay will be shown.
A user could choose to use a high-refresh and/or VRR display here.
Including a VRR display does not make a difference for this use-case.

There's one more criticism which I've seen thrown around:
"Valve doesn't support their hardware".
I have seen very little evidence to support this.

That "very little evidence": _Steam Machines were a failure._
I mostly agree with the statement itself,
but remember that Valve never produced a Steam Machine of their own.
The hardware manufacturers, not Valve, are responsible for their support.

Let us look at the other hardware Valve has produced:

- **Steam Link**:
Released in November 2015, discontinued in November 2018.
Continues to receive updates to this day
(most recently [a month ago](https://steamcommunity.com/app/353380/discussions/0/3108027565748966372/)).
_Still supported._
- **Steam Controller**:
Released in November 2015, discontinued in November 2019.
The _Steam Controller API_ was rebranded as _Steam Input API_ for use with any controllers.
Steam Input continues to receive updates to this day,
as more controllers (including the ones attached to the Steam Deck) are added.
In 2018, [a driver was added to the Linux kernel](https://lkml.org/lkml/2018/4/16/380)
for use without Steam.
_Still supported._
- **Valve Index**:
Released in June 2019.
_Still supported._

"Discontinued" is absolutely not a synonym of "unsupported".
However, there may be some merit to those who criticize Valve
for not providing a way forward for those who now want a Link
or who need to replace a broken Controller.
Well...

- **Steam Link**:
[The Steam Link now also refers to an application/service](https://store.steampowered.com/app/353380/Steam_Link/)
which can be installed on a variety of platforms (i.e.: _basically anything_):

  > The Steam Link app is free and available for all major devices and platforms.
  > - Windows, Mac, Linux
  > - Apple iPhone, iPad, and Apple TV
  > - Android TV, Tablets, and Phones
  > - Samsung Smart TVs
  > - Raspberry Pi 3, 3+, and 4

- **Steam Controller**:
Steam Input now supports XInput (XBox or similar), DirectInput (DualShock or similar), the Switch Pro controller, and more. (i.e.: _basically anything_)

In both of these cases,
the devices in question are 90% replaceable with whatever you want:
Steam Link with the app on your preferred platform,
and Steam Controller with Steam Input on your preferred controller.
I say "90%" for both for good reason.
The Steam Link (hardware) is a plug-and-play solution which an app doesn't satisfy.
The Steam Controller's touchpads offer unique input methods which no other controller has.

## Final Thoughts

The Steam Deck is the culmination of over a decade of Valve investing in Linux.
Since [the first announcement in 2010](https://www.phoronix.com/scan.php?page=article&item=valve_steam_announcement&num=1%22),
Valve has been at the forefront improving gaming on Linux.
Steam on Linux,
all of Valve's games for Linux,
Source Engine for Linux,
three iterations of a Linux-based OS,
Proton,
and finally a hybrid console-like device running Linux.

I am excited for many reasons, but most of all: **It's a PC.**

