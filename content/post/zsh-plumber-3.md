---
title: "Zsh Plumber: Prototype 2.1"
date: 2020-04-08T17:02:45-05:00
tags: []
---

_[all posts in this series](/tags/zsh-plumber)_

[The third prototype is done, and pretty functional](https://github.com/xPMo/zsh-plumber/tree/proto-3).

It is more of a smaller iteration on prototyle 2.

<!--more-->

We do abuse `eval` to start our initial command without pushing a huge command into the BUFFER,
which shouldn't have to many consequences,
unless the user is using `$funcstack`,
or `return` at the top level.

This is better, since it will kill the background processes at the end.
I would call this functional enough for experienced use,
you probably won't run into too many bugs.

Some TODOs:

- Add exit traps for cleanup in case the user exits the shell before finishing the pipe
- Prepare some README for prompt integration using `$_ZPLUMBER_START_BUFFER`.
- Fix out of date tests.
It will be difficult to test now that we are using `eval`,
so hopefully I can find a different way to run commands that will fix the `return`/`$funcstack`/etc. problems.
