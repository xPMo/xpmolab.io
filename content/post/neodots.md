---
title: "Neodots"
date: 2019-01-23T10:01:22-06:00
tags: []
---

Ah, dotfile management...
Tools like [GNU Stow](https://www.gnu.org/software/stow) for symlink management
and tools like [Chezmoi](https://github.com/twpayne/chezmoi) for templating.

Everyone has their own solution, and none of them are for me.

<!--more-->

When building my own solution, I valued a few things:

- Use Git.
- Keep things clean. If I don't need some of my config, there's no need for it to be on my machine.
	- This goes for my scripts as well. I don't need a screenshotting script on my headless Pi.
- I can share my dotfiles without revealing any private information within them.
- _Don't_ use templating.
	- I want to edit my config files as they exist at their destination.
	- I want to immediately see the resulting changes.

My solution? Using Git submodules. [Thus, Neodots is born](https://gitlab.com/gamma-neodots).

This is a submodule- and symlink-based dotfile project.
I discovered Git submodules when I found a project which asked me to `clone --recursive`.
Submodules, in short, are a way to bring in dependencies as a reference to another git repository.
[Read more at the Neodots Wiki](https://gitlab.com/gamma-neodots/neodots/wikis/Home).
