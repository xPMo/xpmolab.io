---
title: "Private Git Remote"
date: 2019-01-27T14:08:00-06:00
draft: true
tags: []
---

Maybe you're trying to separate yourself from GitHub after the recent Microsoft acquisition.
Maybe you never use the issue, pull request, or wiki features of GitHub or other services.
Maybe you need a lot of storage for git-lfs or git-annex.
Maybe you just need command line access to a project of yours.

Whatever your usecase, you're here because you want to setup an ssh-only git remote
on your own private server.
Let's do this.

<!--more-->

So the first thing we need is a server.
I will be using my own personal Raspberry Pi for this guide.
I highly recommend setting up your ssh config to make remoting in easier.

I will be using the same key to connect to my own user as `git`, just to make things simpler to setup.

```ssh_config
# me@remote
Host pi
	HostName 192.168.x.x
	User me
	IdentityFile ~/.ssh/id_ed25519
# git@remote
Host gpi
	HostName 192.168.x.x
	User git
	IdentityFile ~/.ssh/id_ed25519
```

So the first thing to do is create the git user on the remote server.
You can specify any home directory you wish but it is common to put git's home at `/srv/git`:

```
me@mylocalmachine$ ssh pi
me@remote$ sudo useradd -d /srv/git git
me@remote$ mkdir -p /srv/git/.ssh
me@remote$ grep 'me@mylocalmachine$' ~/.ssh/authorized_keys > ~git/.ssh/authorized_keys
me@remote$ sudo chown -R git:git ~git
me@remote$ sudo chmod -R o=,g= ~git
```
