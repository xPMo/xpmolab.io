---
title: "Ironing Out the Wrinkles"
date: 2022-06-28T10:02:01-05:00
draft: false
tags: ["tailscale", "firewalld", "linux", "tweaking"]
---

So I just re-installed ~~Arch~~ EndeavourOS on both my laptop and desktop
and decided it was time to do some much needed re-architecting.
I also wanted to keep track of what changes I made in this post
so I can maybe someday make an Ansible playbook to set these things up.

A few new things this time:

- I'm starting from EOS KDE edition, switch to KDE utils
- EndeavourOS installs firewalld, let's learn to use it
- Tailscale

<!--more-->

# Problem 1: Firewalld and Tailscale

Tailscaled sets up the `tailscale0` interface,
and since it is not managed by NetworkManger,
it doesn't integrate into firewalld directly.

## Solution:

`/usr/local/lib/system/tailscale-firewalld.service`

```ini
[Unit]
Description=Update tailscale's firewalld zone after its interface comes online
Requires=tailscaled.service
After=tailscaled.service

[Service]
ExecStart=firewall-cmd --zone=trusted --add-interface=tailscale0

[Install]
WantedBy=multi-user.target
```
---

```sh
systemctl daemon-reload
systemctl enable --now tailscale-firewalld
```

# Problem 2: profile sync daemon

Profile sync daemon is a daemon which keeps a browser's profile in tmpfs to speed it up.

However, if you have multiple profiles or a lot of profiles, it can use a lot of RAM.

## Solution 1:

Let's increase our runtime directory size.

`/etc/systemd/logind.conf`

```ini
[Login]
# ...
RuntimeDirectorySize=15%   # Increased from 10%
```

## Solution 2:

Reads will be cached perfectly fine by the browser and kernel, let's only worry about writes.
Use the overlay filesystem.

`/etc/sudoers.d/20-psd-overlay`

```sudoers
%users ALL=(ALL)NOPASSWD: /usr/bin/psd-overlay-helper
```

`~/.config/psd/psd.conf`

```
USE_OVERLAYFS="yes"
```

