---
title: "Its Nice"
date: 2022-06-21T21:35:54-05:00
draft: false
tags: ["linux", "arch"]
---

It sucks when your laptop breaks.
But it's nice when you immediately know how to fix it.

Today that happened to my Arch laptop (either a grub config or kernel install problem.

All it took was:

<!--more-->

1. Liveboot an Arch-based distro (EndeavourOS)
2.

```sh
lsblk --fs
cryptsetup open /dev/sda3 root
mount -o subvol=@ /dev/mapper/root /mnt
arch-chroot /mnt bash -c 'mount -a && pacman -S linux-zen'
```

Fin.
