---
title: "Fedora Toolbox on Arch"
date: 2021-02-26T16:35:55-06:00
draft: true
tags: ["linux","tutorial"]
---

Linux users often have 

<!--more-->

## Setting up podman and toolbox

```bash
# pulls in podman and flatpak
sudo pacman -Syu toolbox
```

I run my machine on btrfs,
and I use [Snapper][snapper] to create snapshots of my home directory.
To explicitly exclude podman containers from these snapshots,
I created a subvolume for them:

```bash
# Since I own this path, I don't need root
btrfs subvol create ~/.local/share/containers
```

Additionally, I set my storage driver in `/etc/containers/storage.conf`:

```
driver = "btrfs"
```

For my user's podman containers to work without root,
my user needs to be able to create [subordinate UIDs][subuid] and GIDs.

```bash
# https://wiki.archlinux.org/index.php/Podman#Rootless_Podman
sudo touch /etc/sub{u,g}id
sudo usermod --add-subuids 165536-231072 --add-subgids 165536-231072 $USER
```

```
toolbox create --image registry.fedoraproject.org/fedora-toolbox:34
```

## Limitations of toolbox

Fedora has containers prepared for `toolbox`,
but other distros have to create their own.
I looked into [buildah][buildah] to modify existing containers to work with toolbox,
but the [image requirements](https://github.com/containers/toolbox/#image-requirements) toolbox provides seem to be incomplete.
Even [building the Arch image from this recent PR](https://github.com/containers/toolbox/pull/710) failed.
Perhaps in the near future other distros will develop and maintain their own toolbox images
and this problem will be solved.

In the meantime, I want to run more distros!

## Enter tlbx

After some digging I came across [this issue](https://github.com/containers/toolbox/issues/350)
which led me to find a more general runner called [tlbx][tlbx].

For my Arch host, I needed to remove the `/media` line from the script,
but then this just worked:

```bash
./tlbx create -i registry.hub.docker.com/library/archlinux:latest
./tlbx enter archlinux-tlbx
```

And I was in a mutable Arch container!

To make it easier to install packages,
[I re-logged in with `--user=root` and installed sudo](https://gitlab.com/uppercat/tlbx/-/wikis/troubleshooting#user-content-how-am-i-supposed-to-install-packages-it-says-it-requires-superuser-privilege-but-i-cant-become-root).

I'm looking forward to trying some other distros this way.

## Conclusion

Leveraging containers and overlay filesystems will be a gamechanger for how I try out distributions.

My fork of tlbx can be found [here][my-tlbx].

[snapper]: https://wiki.archlinux.org/index.php/Snapper
[subuid]: https://man.archlinux.org/man/subuid.5
[buildah]: https://github.com/containers/buildah
[tlbx]: https://gitlab.com/uppercat/tlbx
[my-tlbx]: https://gitlab.com/xPMo/tlbx
