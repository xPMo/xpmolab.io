---
title: "Zsh Plumber: Prototype 1"
date: 2020-04-07T10:30:58-05:00
tags: ["zsh","zle","zsh-plumber"]
---

_[all posts in this series](/tags/zsh-plumber)_

[The first prototype is done](https://github.com/xPMo/zsh-plumber/tree/proto-1).

<!--more-->

This implementation:

- Overrides the default `accept-line`
- If a trailing `|` is found:
	- it is removed and replace with `> TMPFILE &`
	- a self-removing hook is added to `line-init` which initializes the buffer with `< TMPFILE`

That's it.
This prototype was meant to find a way to interact with the line editor.
This method is very clear, since it puts the file the command will be reading from into the BUFFER.
However, there needs to be file cleanup,
and the ability to switch from a file to a pipe
would prevent long-running pipelines from making enormous files.

I'll probably tackle that next.
Until I understand how to manipulate the history subsystem better,
the `$BUFFER` will just have to be messy from all the replacements.
