---
title: "100 Days of Code: Day 01"
date: 2020-01-02T08:38:45-06:00
tags: ["C++","100-days-of-code"]
---

<!--more-->

One of the most popular languages out there is C++.
I have next to no experience in it, but I might as well try.

Today's project was hacking on [Waybar](https://github.com/Alexays/Waybar),
regarding [this issue](https://github.com/Alexays/Waybar/issues/255).

[My changes wouldn't build](https://github.com/xPMo/Waybar/commit/585dff929168ea576b02c3d3b00b22fb7ddc1cf7),
but I'm hoping to get feedback or have someone else fix them.
Either way, it will help me learn how namespaces work in C++,
and contribute to this project in the future.


