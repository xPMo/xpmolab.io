---
title: "Installing Arch Linux (Again)"
subtitle: Two disks, six mount points
date: 2018-07-23
tags: ["linux", "rambling"]
---

For all my undergrad, my laptop was a refurb'd EliteBook 2540p.
It was released in 2010 around the $1000 mark, and I picked it up in the summer of 2014 for $300.
It was a little 12-inch thing, came with Windows 7 and no bloatware to speak of.
I loved that thing, despite the overheating, the weight, the first-gen Intel Core series chip, the pathetic battery life, and the overheating.

<!--more-->

The HDD died, it was replaced by a SSD.
That was hard to find, since it was a 1.8" bay.
Ended up going with a tiny 60GB one.
As I finished up my senior year, there were chips and cracks all over its plastic surface, the lid hatch had long since failed, and one of mouse buttons didn't work (there was a second pair though).

That ancient i7 had seen many operating systems. Windows 7, Windows 10, Ubuntu, Debian, OpenSUSE, Slax, Porteus, Antergos, Arch, and Manjaro.

The integrated graphics was failing, I had to add `nomodeset` to my kernel parameters.
This made the screen jittery when scrolling some web pages, and amplified the CPU overheating issues.

The battery was at EOL (again), and finally the power cord stopped working.  It was time.

Amazon's Prime Day was at hand, and I had a new laptop to buy.  I settled on an ASUS VivoBook F510UA.
8GB RAM, a 128GB SSD, a 1TB HDD, 15.6" screen.
Standard mid-range specs, reasonably thin, reasonably light.
I wanted something that wouldn't overheat, and I could work comfortably with for about 8 years.
This was twice the price of my old laptop, it's only fair that it should last twice as long.

## Booting up

Cortana speaks out of the void, and I let it boot up.
I spent a few minutes with Windows 10, searched for Chocolatey, checked out the privacy settings, and then just give up.
I haven't used Windows since sophomore year, why should I need it now?
It had been a while since I installed Arch, so I almost went with Manjaro to make things easier.
But, I stayed my hand on the `dd` command, and changed the iso to Arch.
I had some time to spare, I've got time to do it right.

With the wiki up on my phone, I got to work.
Standard affair throughout, except for an unorthodox disk setup:

```fstab
# <file system> <dir> <type> <options> <dump> <pass>
# UUID=A3D9-FBF3
/dev/sdb1           	/boot     	vfat      	rw,relatime,fmask=0022,dmask=0022,codepage=437,iocharset=iso8859-1,shortname=mixed,utf8,errors=remount-ro	0 2

# /dev/mapper/cryptoroot
UUID=5a909113-d359-447f-9dc1-edd220bd8454	/         	btrfs     	rw,relatime,compress=lzo,ssd,space_cache,subvolid=257,subvol=/@,subvol=@	0 0

# /dev/sda1
UUID=e5fb9516-ff7c-4415-bdfc-563b15bcfd83	/home/pmo/.local/share/games2	btrfs     	rw,relatime,compress=zstd,space_cache,subvolid=258,subvol=/@/games,subvol=@/games	0 0

# /dev/sda1
UUID=e5fb9516-ff7c-4415-bdfc-563b15bcfd83	/usr/local/share/music	btrfs     	rw,relatime,compress=zstd,space_cache,subvolid=260,subvol=@/music	0 0

# /dev/sda1
UUID=e5fb9516-ff7c-4415-bdfc-563b15bcfd83	/usr/local/share/videos	btrfs     	rw,relatime,compress=zstd,space_cache,subvolid=261,subvol=@/videos	0 0

# /dev/sdb2
UUID=6140bd5c-a328-44d6-a5a9-50b3b6ea9814	none      	swap      	defaults,pri=-2	0 0
```

My root partition is a subvolume of an encrypted btrfs filesystem,
and my 1TB disk is split out with subvolumes and mounted a few different spots.

I've installed the necessities and an AUR helper.

I've copied my old laptop ssh keys from my old laptop drive (now mounted in my desktop), and cloned my dots.

After a few short hours of work, everything's golden.

{{<figure src="/img/2018-07-23_22:25:07.vDJ.png" title="Yes, the wallpaper is Ubuntu. Fight me.">}}
