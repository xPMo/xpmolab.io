---
title: "Zsh Plumber: Prototype 2"
date: 2020-04-07T15:20:15-05:00
tags: ["zsh","zle","zsh-plumber"]
---

_[all posts in this series](/tags/zsh-plumber)_

[The second prototype is done, but buggy](https://github.com/xPMo/zsh-plumber/tree/proto-2).

<!--more-->

I've changed a few things:
Most notably, this no longer overrides accept-line.
Instead, this uses ZLE hooks to modify the buffer right before it is executed.
Whether the `$BUFFER` will still be used in the end is TBD.

This now writes to a temporary pipe, which is handled by:

```zsh
(
	cat >$FILE &
	pid=$!
	zselect -r $CTRL_FD
	kill $pid
	cat >$OUT_FD
) <&$IN_FD &
```

I'm having a hard time getting both the initial (pre-pipe) process
and the zselect-based-switcher to die when the final process terminates, though.

