---
title: "De-Googling my OnePlus 5T with LineageOS"
date: 2020-09-02T11:36:03-05:00
tags: ["Android", "privacy"]
---

## Privacy Matters

I am known among friends and family to be a privacy-concious person.
My opinion on privacy matters is that you should be aware of what you are paying with your data,
then make the decision that makes the most sense to you.

<!--more-->

If a business is making money [sending chat messages between your friends for free][hangouts],
then [someone is paying for those messages][hangouts-privacy].  
If a business is making money [hosting your email][gmail],
then [someone else is reading your email][gmail-privacy].  
If a business is making money [tracking your location][gmaps],
then [someone is paying for where you are][gmaps-privacy].  
If a business is making money [hosting web content for free][amp],
then [someone else is suffering because of that][amp-privacy].

Now I want to emphasize the following:
**This is not a condemnation of Google services**
(except for AMP, AMP is awful).
Google has been a ~~good~~ _okay_ steward of private information,
and the services they provide are extremely convenient.
I simply want to use my experience with microG/LineageOS
to find out exactly how much convenience and/or money is sacrificed
to restore control over your mobile data.

[amp]: https://developers.google.com/amp "Google's Internet"
[amp-privacy]: https://medium.com/@danbuben/why-amp-is-bad-for-your-site-and-for-the-web-e4d060a4ff31
[gmaps]: https://www.google.com/maps "Google's Maps"
[gmaps-privacy]: https://arstechnica.com/tech-policy/2020/08/unredacted-suit-shows-googles-own-engineers-confused-by-privacy-settings
[hangouts]: https://hangouts.google.com "Google's Hangouts"
[hangouts-privacy]: https://bgr.com/2015/05/12/google-hangouts-encryption-privacy-security
[gmail]: https://www.google.com/gmail/about "Google's Mail"
[gmail-privacy]: https://fortune.com/2018/09/21/google-gmail-privacy-data-third-parties "GMail Privacy"

# What is LineageOS?
 
[LineageOS][lineage] is an Open-Source Operating System
based on the Android Open-Source Project (AOSP),
and spiritual successor of the [CyanogenMod project][wiki-cyanogen].

LineageOS comes bundled with a number of patched versions of AOSP applications,
such as a launcher (Trebuchet, forked from CyanogenMod's Catapult),
a music player (Eleven, forked from CyanogenMod's project of the same name),
a messaging application, or a clock.
The project supports [a few addons][lineage-extras]
like [OpenWeatherMap][owm] as a weather provider
or an `su` program so applications can get root permissions.
Privacy Guard is an application which can track and limit application permission access
(such as location information).
The project also develops some APIs for application developers to use
(often in-place-of similar APIs provided by Google in non-AOSP Android builds).

The project is well-repsected,
especially as a way to increase the longevity of devices unsupported by the original vendors.

[wiki-cyanogen]: https://en.wikipedia.org/wiki/CyanogenMod "CyanogenMod on Wikipedia"
[lineage]: https://lineageos.org/ "LineageOS"
[lineage-extras]: https://download.lineageos.org/extras "LineageOS Extras"
[lineage-install]: https://wiki.lineageos.org/devices/dumpling/install "LineageOS Install Guide for OnePlus5T"

# Why Android is Proprietary

By default, there are no Google services included (no Play Store, no Google, etc).
There is a link to the [OpenGApps project][ogapps],
which provides stock Google applications, however:

_Google Play services_ is a proprietary service which provides APIs for Android applications.
These APIs include Google SSO, Google Cast, Analytics, Google Wallet, Google Maps, Google Play Games, and Google Mobile Ads.
Without Google Play services, many applications (including all of Google's) will not function.

Every package from OGApps includes Google Play and Google Play services.
Even the "pico" package.
However, we don't have to forgo most of the Play library just to avoid Google Play services.
Instead, [microG][microg] is an open-source system-level application which reimplements some APIs normally provided by Google Play services.

# The actual image

The actual image I flashed to my phone is
offered by the [LineageOS for microG project][lineage-microg].
It provides two key features for getting convenience back into a Google-free Android.

- It includes microG, so more applications will work without Google Play services
- It includes [F-Droid][fd] _and_ the [F-Droid Priviledged Extension][fd-pe]

If you have ever used [F-Droid][fd],
one of the biggest papercuts you've faced is in updating applications.
If applications need updates, you must

1. Open F-Droid
2. Select the Updates tab
3. Tab "Update All"
4. Wait for an app to download
5. Tap install for that app
6. Tap install on the "are you sure?" dialog that pops up
7. Repeat 4-6 for every application you are updating.

With the privileged extension, F-Droid works like Google Play normally does.

1. Open Settings
2. Turn on "Automatically Install Updates"

_That's it._ F-Droid applications will be updated in the background from here-on-out.

[ogapps]: https://opengapps.org/aboutsection "OpenGApps Project"
[microg]: https://microg.org/ "About the MicroG Project"
[lineage-microg]: https://lineage.microg.org/ "LineageOS for microG"
[fd-pe]: https://f-droid.org/en/packages/org.fdroid.fdroid.privileged "F-Droid Privileged Extension"
[fd]: https://f-droid.org/ "Free and Open Source App Repository"

# Install Process

While my image was from the [LineageOS for microG project][lineage-microg],
I followed [LineageOS's install instructions for my device][lineage-install].

After sideloading the system image,
I sideloaded [LineageOS's AddonSU][lineage-extras] to root my device.

I rebooted to the system image, and walked through the setup without issue.
Easy. I now have a Google-free Android device!

# Application Shortlist

F-Droid has their own repository with curated applications,
but there are some notable third-party repositories as well.

- [BitWarden][fd-bw] - A repository for the [BitWarden Password Manager][bw]
- [NewPipe][fd-np] - A repository for [NewPipe, a YouTube/PeerTube/SoundCloud client][np]
- [Ricki Hirner's Repository][fd-ff] - A repository for Firefox, Telegram, and other notable applications

Because this post has "Google" in the title,
here are some applications which can (to some extent) replace Google applications or services:

- [andOTP][fd-andotp] - A One-Time-Password generator.
- [Syncthing-Fork][fd-stf] - A fork of the [Syncthing][st] client for Android.
- [OpenWeatherMap][lineage-extras] - A weather provider
- [NewPipe][np] - Already mentioned, a YouTube client
- [Markor][fd-markor] - A note-taking application
- [Aurora Store][fd-aurora] - A Google Play Store client

I will admit, there are two **big** holes in this list,
and they are difficult holes to fill.

[fd-andotp]: https://f-droid.org/en/packages/org.shadowice.flocke.andotp/ "andOTP on F-Droid"
[fd-aurora]: https://f-droid.org/packages/com.aurora.store "Aurora Store on F-Droid"
[fd-bw]: https://mobileapp.bitwarden.com/fdroid/repo?fingerprint=BC54EA6FD1CD5175BCCCC47C561C5726E1C3ED7E686B6DB4B18BAC843A3EFE6C "BitWarden: F-Droid Repository"
[fd-ff]: https://rfc2822.gitlab.io/fdroid-firefox/fdroid/repo?fingerprint=8F992BBBA0340EFE6299C7A410B36D9C8889114CA6C58013C3587CDA411B4AED "Unofficial Firefox F-Droid Repository"
[fd-np]: https://archive.newpipe.net/fdroid/repo/?fingerprint=E2402C78F9B97C6C89E97DB914A2751FDA1D02FE2039CC0897A462BDB57E7501 "NewPipe: F-Droid Repository"
[fd-stf]: https://f-droid.org/packages/com.github.catfriend1.syncthingandroid/ "Syncthing-Fork on F-Droid"
[fd-markor]: https://f-droid.org/packages/net.gsantner.markor "Markor on F-Droid"
[st]: https://syncthing.net/ "Syncthing: A Continuous File Synchronization Program"

# Application Shortcomings

There is no good replacement for [Google Assistant][at-ga].
Speech-to-text services are notoriously memory- and storage-heavy.
If you don't send your voice to Google, you are sending it to someone else.
[Mycroft][mycroft] does not officially support Android yet,
[but does have an Android repository][mycroft-and].
However, it requires Mycroft-core (the actual speech recognition server)
to be hosted elsewhere.

Finally, there is no easy way to replace [Google's _Find My Device_][at-fmd]
(Formally _Android Device Manager_).
You need to have your phone broadcast its location to some service,
and allow that service to remotely ring, lock or wipe your device.
The biggest use I get from this is remotely ringing my device if I've misplaced it at home.

> **Addendum 2021-03-10:** I have since found an [altenative,
appropriately named "FindMyDevice"](https://f-droid.org/en/packages/de.nulide.findmydevice/).
It reads SMS messages for a predefined string (like "fmd") from a whitelisted number.
It needs an `adb` command if you want it to be able to turn on location,
but everything else is pretty straightforward.

As a stopgap, [KDEConnect][fd-kdec] can let me remotely ring my device while connected to the same network.
Perhaps with [WireGuard][fd-wg] and a \$5/mo VPS, I can ring it from anywhere?
Then add in [Termux][fd-termux] for the rest of the remote control?
More research is needed.

[at-ga]:https://alternativeto.net/software/google-assistant?platform=android
[at-fmd]: https://alternativeto.net/software/android-device-manager?platform=android
[fd-kdec]: https://f-droid.org/packages/org.kde.kdeconnect_tp "KDE Connect on F-Droid"
[fd-wg]: https://f-droid.org/packages/com.wireguard.android "WireGuard on F-Droid"
[fd-termux]: https://f-droid.org/packages/com.termux "Termux on F-Droid"
[wg]: https://www.wireguard.com/ "WireGuard: Fast, Modern VPN Tunnel"
[mycroft]: https://mycroft.ai "Mycroft: The Open-Source Privacy-Focused Assistant"
[mycroft-and]: https://github.com/MycroftAI/Mycroft-Android

# Absolutely Proprietary

There are some crucial services I rely on which are only available on the Google Play Store.

![Have a look inside my proprietary folder](/img/and-prop-apps.jpg)

For now, this is it, but I will probably add a couple more.

- My family uses GroupMe to chat.
I want to set up a [Matrix puppet][mpuppet] so I can just use Element instead,
but it seems that [the GroupMe puppet][gmpuppet] is currently broken. ☹
Maybe I can convince my family to use a Matrix/Element stack instead?
- I use Walgreens as my pharmacy.
- I use ZeroTier as a faster option to a traditional VPN, since it's P2P.
- I use Tile because I will misplace my keys at least once a week.
- I have some Android games on HumbleBundle.
- [Forest][forest] is a very good live wallpaper,
has no ads,
and shows weather and cloud cover to match your location
with an OpenWeatherMap key.
The only in-app purchases are for donations to the author.


# Root Access

Root access is not enabled by default,
even with [LineageOS's AddonSU][lineage-extras].
To actually enable root access,
you must first enable Developer Options
(About Phone > Tap "Build number" repeatedly),
_then_ in System > Advanced > Developer Options > Root access,
enable root access from either Apps, ADB, or both.

![Just hit that button, okay?](/img/and9-root.jpg)

The "Manage root accesses" option below "Root access"
is for LineageOS's Privacy Guard's controls.
It controls root access on an app-by-app basis,
and logs what apps have asked for it.


[mpuppet]: https://matrix.org/bridges "All the Matrix Bridges"
[gmpuppet]: https://github.com/matrix-hacks/matrix-puppet-groupme
[andotp]: https://github.com/andOTP/andOTP "andOTP on GitHub"
[bw]: https://bitwarden.com/ "BitWarden: Open Source Password Manager"
[element]: https://element.io "Element Matrix Client"
[forest]: https://play.google.com/store/apps/details?id=kaka.wallpaper.forest "Forest Live Wallpaper on Google Play"
[kdec]: https://kdeconnect.kde.org/ "KDE Connect"
[np]: https://newpipe.schabi.org/ "NewPipe: Lightweight YouTube experience for Android"
[owm]: https://home.openweathermap.org/ "OpenWeatherMap"
