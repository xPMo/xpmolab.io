---
title: "Reflecting on a Compliment"
date: 2020-02-20T14:28:10-06:00
tags: ["life","story"]
---

Sometimes in your life it takes someone else to get you to realize just how much you know.

This is a story of about two hours of my life on February 20th, 2020.

> Are you a single person?

<!--more-->

Yesterday I saw [a question on /r/termux](/r/termux/comments/f6db3q/ls_with_color/)
which was intriguing.
I pulled up Termux (for the first time in a long time)
to verify the post,
and then on a hunch [dug into termux/termux-packages to find the answer](https://github.com/termux/termux-packages/blob/master/packages/coreutils/src-ls.c.patch).
While I had Termux up, I went ahead and pulled down system updates with apt,
and updates to all my git repos.

This morning I woke up and had some eggs and bacon.
It's Thursday, I have office hours this afternoon and tests to grade.

Due to recency bias, I pulled up Termux again.
I opened it to something strange.
[Zinit](https://github.com/zdharma/zinit/)
wasn't printing plugin-loading messages,
and none of my plugins were loading
(aside from [powerlevel10k](https://github.com/romkatv/powerlevel10k),
which is loaded as a snippet).

I really want my [syntax highlighting](https://github.com/zdharma/fast-syntax-highlighting),
[keybindings](https://github.com/zsh-vi-more), et cetera;
so I `cd ~/Repos/zinit` to checkout an earlier commit.

Zinit used to be called Zplugin,
and I had done the name conversion successfully already,
so I use `git l | rg 'zinit'` to find a sufficiently old commit
that is after the name change.

I verify that everything works, and then check the time.
It's about 11am, I don't have my office hour until 1pm.
I have a bad commit (master) and a bad commit (where I am).
So I `git bisect bad @{0}; git bisect good master`.

Using `git bisect` entails
letting `git` checkout a commit between a good and bad commit,
and then you marking it as good or bad.
On each iteration,
I created a new `tmux` pane
and saw if plugins got loaded by Zinit.
If they did, `git bisect good`;
if they didn't, `git bisect bad`.
`exec zsh` and check the next commit.

The bisecting session ends with the following as the offending commit:

```
commit f6145dff242c5684f7abea2c26b3c9f184eaa85b
Author: SG <sgniazdowski@gmail.com>
Date:   Wed Jan 29 14:20:37 2020 +0100

    zinit: Use zselect for a more predictable sleep time

 1 file changed, 4 insertions(+), 3 deletions(-)
```

After `git show f6145df`,
I see that Zinit broke when the project loaded the zsh module `zsh/zselect`.

[I knew from 18 months ago that modules on Termux are staticly compiled](https://github.com/termux/termux-packages/issues/2829),
so I try and `zmodload zsh/zselect`.
It fails, and [I open an issue against Termux packages](https://github.com/termux/termux-packages/issues/4939)
and [another for reference against Zinit](https://github.com/zdharma/zinit/issues/267).

It is about half-past 11 at this point,
I still have some time to kill.
So I check [past closing commits,
and realize that the fix is just adding the module's name in a Bash script
and bumping the version appropriately](https://github.com/termux/termux-packages/commit/68892549829968117716e907f3c21183fa0c6f48).
I can do that, [so I do](https://github.com/termux/termux-packages/pull/4940).

At this point romkatv (the developer of powerlevel10k,
another plugin which uses on zselect)
and psprint (the developer of Zinit)
have both chimed in on the issue, 
so I mention I made a PR.
Romkatv thanks me on the PR,
and I decide to pack up and walk to campus.

When I get there,
I see that my PR has been merged,
and I `apt update` on my phone.
No updates.

I jump back to the home page for _Termux packages_ and check the Actions tab.
There is a running action for my PR,
so I wait and start ~~grading~~ browsing Reddit.

I try again after a bit and I pull the new update.
I verify that `zsh/zselect` works,
make a comment on the Zinit issue,
and get back to business.

At this point,
my issue on _Termux packages_ has a bunch of referenced actions--
all by me (a reference from the Zinit issue,
two commits which refer to it,
making a pull request which closes it).
I think it may have been this wall of my actions on a completely foreign repository
which prompted the highlight of this story:

{{<figure src="/img/zinit-267-compliment.png">}}

----

Reflecting on all of this,
there was a lot of prior knowledge I needed.

- [I first used `git-bisect` to figure out what caused a partial i3 regression](https://github.com/i3/i3/issues/3506#issuecomment-437620798)
- [I knew about Termux's static approach to zsh modules from a previous issue I reported](https://github.com/termux/termux-packages/issues/2829)
and [an issue I found while looking for solutions](https://github.com/termux/termux-packages/issues/61)
- I knew how the version of a package should be changed from years of using package managers on Linux.
- I found out where to put the revision number in Termux's build system
by [looking through commits](https://github.com/termux/termux-packages/commits/master)
until [I found a commit which did so](https://github.com/termux/termux-packages/commit/d665dc8ae9f13eb2b05783fc4463b014d35a5b35).

And the whole process took about 40 minutes total out of my day,
over the course of a couple of hours.

I was solving a problem I had;
but also solving a problem that was relevant to others.
I am highly motivated by helping others;
it's why I spend so much time on help forums answering questions;
it's why I love teaching.
A small compliment like this can make someone's day,
so don't be afraid to give out a cheesy compliment.
They will probably appreciate it.

They may write a blog post about it.
